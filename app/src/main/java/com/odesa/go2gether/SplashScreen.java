package com.odesa.go2gether;

//import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //   اردت أن يتصل مع واجهة لياوت
        setContentView(R.layout.activity_splash_screen);


        /****** انشاء ثريد لجعل التطبيق ينام لمدة 5 ثواني  *************/
        Thread background = new Thread() {
            public void run() {

                try {

                    sleep(5 * 1000); // غير في الرقم 5 لتغيير عدد الثواني .. ثواني الانتظار قبل الدخول للتطبيق

                    // بعد 5 ثواني نفذ التالي وهو الانتقال بنا الى شاشة تحديد نوع المستخدم سائق أم زبون
                    Intent i = new Intent(getBaseContext(), UserType.class);
                    startActivity(i);

                    // اغلاق شاشة السبلاش بشكل كلشي بعد الانتقال
                    finish();

                } catch (Exception e) {

                }
            }
        };

        // تشغيل الثريد السابق
        background.start();
    }}
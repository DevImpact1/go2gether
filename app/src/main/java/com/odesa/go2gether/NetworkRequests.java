package com.odesa.go2gether;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class NetworkRequests {
    String urlLoginClient = "https://go2gether.000webhostapp.com/api/login_client.php"; // رابط API تحقق من بيانات زبون
    String urlLoginDriver = "https://go2gether.000webhostapp.com/api/login_driver.php"; // رابط API تحقق من بيانات زبون

    Context context;
    static boolean loginSuccess = false;

    // الباني
    public NetworkRequests(Context context) {
        this.context = context;
    }

    // التحقق أونلاين من صحة بيانات المستخدم
    void loginUser(final String mobileNum, final String password, final String userTupe, final VolleyCallback callback) {

        RequestQueue queue = Volley.newRequestQueue(context);
        // Post params to be sent to the server
        // تمرير البيانات للسيرفر
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", mobileNum); // "abod@dd.d"
        params.put("password", password); //"1234"

        String url;
        if (userTupe.equalsIgnoreCase("driver")) {
            url = urlLoginDriver; // تحقق من حساب السائق على الرابط التالي
        } else {
            url = urlLoginClient; // تحقق من حساب الزبون على الرابط التالي
        }

        JsonObjectRequest request_json = new JsonObjectRequest(url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    // عند نجاح الإتصال ووصول الرد
                    @Override
                    public void onResponse(JSONObject response) {
                        loginSuccess = true;
                        //Toast.makeText(context, "onResponse" + response, Toast.LENGTH_LONG).show();
                        callback.onSuccess(loginSuccess);
                    }
                }, new Response.ErrorListener() {
            // عند فشل الإتصال
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(context, "onResponse" + error, Toast.LENGTH_SHORT).show();
            }
        });

        // add the request object to the queue to be executed
        queue.add(request_json);
        callback.onSuccess(loginSuccess); // تمرير النتيجة لأكتيفيتي تسجيل الدخول
    }

    void signupUser(){

    }
}

package com.odesa.go2gether;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    String userType = UserType.userType;
    EditText et_mobile_num;
    EditText et_password;
    Button b_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();// تهيئة حقول الإدخال
        finish_tutorial(); // إغلاق أكتيفيتيس العرض الأول
    }

    // تهيئة حقول الإدخال
    private void init() {
        et_mobile_num = (EditText) findViewById(R.id.et_mobile_num);
        et_password = (EditText) findViewById(R.id.et_password);
        b_login = (Button) findViewById(R.id.b_login);

        et_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    b_login.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    // إغلاق أكتيفيتيس العرض الأول
    private void finish_tutorial() {
        try {
            if (userType.equalsIgnoreCase("driver")) {
                tutorialDriverActivity_1.context.finish();
                tutorialDriverActivity_2.context.finish();
                tutorialDriverActivity_3.context.finish();
            } else if (userType.equalsIgnoreCase("client")) {
                tutorialClientActivity_1.context.finish();
                tutorialClientActivity_2.context.finish();
                tutorialClientActivity_3.context.finish();

            }
        } catch (NullPointerException exption) {

        }
    }

    // الإنتقال إلى واجهة إعادة ضبط كلمة السر
    public void RestPassword(View view) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    // الإنتقال إلى واجهة تسجيل حساب جديد
    public void Signup(View view) {
        // تسجيل حساب سائق
        if (userType.equalsIgnoreCase("driver")) {
            Intent intent = new Intent(this, RegisterDriverActivity.class);
            startActivity(intent);
        } else { // تسجيل حساب زبون
            Intent intent = new Intent(this, RegisterClientActivity.class);
            startActivity(intent);
        }
    }

    // عند ضغط زر تسجيل الدخول
    public void loginButton(View view) {
        String mobileNum = et_mobile_num.getText().toString();
        String password = et_password.getText().toString();
        // ضمان أن الحقول غير فارغة
        if (password.length() > 0 && mobileNum.length() > 0) {
            loginUser(mobileNum, password);
        }
    }

    // التحقق من بيانات دخول حساب المستخدم
    private void loginUser(String mobileNum, String password) {
        NetworkRequests networkRequests = new NetworkRequests(this);
        networkRequests.loginUser(mobileNum, password, userType, new VolleyCallback() {
            @Override
            public void onSuccess(boolean loginSuccess) { // تسجيل دخول ناجح
                if (loginSuccess) {//True
                    Intent intent = new Intent(LoginActivity.this, DriverCurrentTrips.class);
                    startActivity(intent);

                }
            }
        });
    }

}

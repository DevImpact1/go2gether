package com.odesa.go2gether;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RegisterClientActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText et_full_name;
    EditText et_email;
    EditText et_country;
    EditText et_phone_number;
    EditText et_password;
    Spinner sp_gender;
    ArrayAdapter<CharSequence> gender_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_client);
        init();  // تهيئة حقول الإدخال

    }

    // تهيئة حقول الإدخال
    private void init() {
        et_full_name = (EditText) findViewById(R.id.et_full_name);
        et_email = (EditText) findViewById(R.id.et_email);
        et_country = (EditText) findViewById(R.id.et_country);
        et_phone_number = (EditText)findViewById(R.id.et_phone_number);
        et_password  = (EditText) findViewById(R.id.et_password);
        sp_gender = (Spinner) findViewById(R.id.sp_gender);
        sp_gender.setOnItemSelectedListener(this);

        // Create an ArrayAdapter using the string array and a default spinner layout
        gender_adapter = ArrayAdapter.createFromResource(this,
                R.array.gender_array, R.layout.spinner_item);

        // Specify the layout to use when the list of choices appears
        gender_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        sp_gender.setAdapter(gender_adapter);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)

        Toast.makeText(this, parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
}
